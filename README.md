# JS-Projectile
An interactive JavaScript/HTML5/SVG based Projectile motion simulator.

See https://phet.colorado.edu/en/simulation/projectile-motion for more details.

Implemented using D3.js


